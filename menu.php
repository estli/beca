<?php 

echo '<div class="navbar">
		<div class="container">
			<div class="panel-control-left">
				<a href="#" data-activates="slide-out-left" class="sidenav-control-left"><i class="fa fa-bars"></i></a>
			</div>
			<div class="site-title">
				<a href="index.php" class="logo"><h1>Becaria.xyz</h1></a>
			</div>
			<div class="panel-control-right">
				<a href="blog.html"><i class="fa fa-rss"></i></a>
			</div>
		</div>
	</div>
	<!-- end navbar -->

	<!-- panel control left -->
	<div class="panel-control-left">
		<ul id="slide-out-left" class="side-nav collapsible"  data-collapsible="accordion">
			<li>
				<a href="index.php"><i class="fa fa-home"></i>Inicio</a>
			</li>
			<li>
				<div class="collapsible-header"><i class="fa fa-file-text-o"></i>Categories<span><i class="fa fa-chevron-right"></i></span></div>
				<div class="collapsible-body">
					<ul class="categories-in collapsible"  data-collapsible="accordion">
						<li><a href="categories.html">Technology</a></li>
						<li><a href="categories.html">Insurance</a></li>
						<li><a href="categories.html">Banking</a></li>
						<li><a href="categories.html">Goverment</a></li>
						<li><a href="categories.html">Marketing</a></li>
					</ul>
				</div>
			</li>
			<li>
				<a href="lista.php"><i class="fa fa-th-large"></i>Listado de Empleos</a>
			</li>
			<li>
				<a href="puesto1.php"><i class="fa fa-list"></i>Detalles del Puesto</a>
			</li>
			<li>
				<div class="collapsible-header"><i class="fa fa-file-text-o"></i>Jobs Pages <span><i class="fa fa-chevron-right"></i></span></div>
				<div class="collapsible-body">
					<ul class="side-nav-panel">
						<li><a href="candidates.html">Candidates</a></li>
						<li><a href="profile-candidates.html">Profile Candidates</a></li>
						<li><a href="employers.html">Employers</a></li>
						<li><a href="profile-employers.html">Profile Employers</a></li>
					</ul>
				</div>
			</li>
			<li>
				<a href="submit-jobs.html"><i class="fa fa-send"></i>Submit Jobs</a>
			</li>
			<li>
				<a href="search-jobs.html"><i class="fa fa-search"></i>Search Jobs</a>
			</li>
			<li>
				<div class="collapsible-header"><i class="fa fa-rss"></i>Blog <span><i class="fa fa-chevron-right"></i></span></div>
				<div class="collapsible-body">
					<ul class="side-nav-panel">
						<li><a href="blog.html">Blog</a></li>
						<li><a href="blog-single.html">Blog Single</a></li>
					</ul>
				</div>
			</li>
			<li>
				<div class="collapsible-header"><i class="fa fa-file-powerpoint-o"></i>Pages <span><i class="fa fa-chevron-right"></i></span></div>
				<div class="collapsible-body">
					<ul class="side-nav-panel">
						<li><a href="faq.php">FAQ</a></li>
						<li><a href="testimonial.html">Testimonial</a></li>
						<li><a href="404.html">404 Page</a></li>
					</ul>
				</div>
			</li>
			<li>
				<a href="contact.html"><i class="fa fa-envelope"></i>Contact Us</a>
			</li>
			<li>
				<a href="login.html"><i class="fa fa-sign-in"></i>Login</a>
			</li>
			<li>
				<a href="register.html"><i class="fa fa-user-plus"></i>Register</a>
			</li>
		</ul>
	</div>';