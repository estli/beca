<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Becaria.xyz - Empleos para Chicas</title>
	<meta name="viewport" content="width=device-width, initial-scale=1  maximum-scale=1 user-scalable=no">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="HandheldFriendly" content="True">
	
	<link rel="shortcut icon" href="img/favicon.png">

	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/materialize.min.css">
	<link rel="stylesheet" href="css/slick.css">
	<link rel="stylesheet" href="css/slick-theme.css">
	<link rel="stylesheet" href="css/owl.carousel.css">
	<link rel="stylesheet" href="css/owl.theme.css">
	<link rel="stylesheet" href="css/owl.transitions.css">
	<link rel="stylesheet" href="css/style.css">

</head>
<body>

	<!-- navbar -->
	<?php include 'menu.php'; ?>
	<!-- end panel control left -->
	
	<!-- slider -->
	<div class="slider-slick app-pages">
		
		<div class="slider-entry">			
			<img src="img/foto1.jpg" alt="">
			<div class="overlay"></div>
			<div class="caption">
				<div class="container">
					<h2>Olvidate de la Crisis</h2>
					<p>Tenemos los mejores formas de ganar dinero</p>
					<a href="faq.php"><button class="button">Leer Mas</button></a>
				</div>
			</div>
		</div>
		<div class="slider-entry">			
			<img src="img/foto3.jpg" alt="">
			<div class="overlay"></div>
			<div class="caption">
				<div class="container">
					<h2>Hagamos un Trato</h2>
					<p>Trabajos acordes a tus condiciones</p>
					<a href="faq.php"><button class="button">Leer Mas</button></a>
				</div>
			</div>
		</div>
		<div class="slider-entry">
			<div class="overlay"></div>
			<img src="img/foto2.jpg" alt="">
			<div class="caption">
				<div class="container">
					<h2>Gana Dinero en tu Tiempo Libre</h2>
					<p>El tiempo es oro y aqui lo tendras en efectivo</p>
					<a href="faq.php"><button class="button">Leer Mas</button></a>
				</div>
			</div>
		</div>
		<div class="slider-entry">
			<div class="overlay"></div>
			<img src="img/foto4.jpg" alt="">
			<div class="caption">
				<div class="container">
					<h2>Encuentra el Trabajo Adecuado</h2>
					<p>Tu eliges que estas dispuesta hacer para ganar dinero</p>
					<a href="faq.php"><button class="button">Leer Mas</button></a>
				</div>
			</div>
		</div>
		<div class="slider-entry">
			<div class="overlay"></div>
			<img src="img/foto5.jpg" alt="">
			<div class="caption">
				<div class="container">
					<h2>Logra tus Metas Facilmente</h2>
					<p>Trabaja desde la comodidad de tu casa</p>
					<a href="faq.php"><button class="button">Leer Mas</button></a>
				</div>
			</div>
		</div>
	</div>
	<!-- end slider -->

	<!-- form search -->
	<div class="form-search">
		<div class="container">
			<div class="form-search-entry">
				<input type="text" placeholder="Palabras Clave">
				<input type="text" placeholder="Ubicacion">
				<select>
					<option value="" disabled selected="">Categorias</option>
					<option value="1">Technology</option>
					<option value="2">Insurance</option>
					<option value="3">Banking</option>
					<option value="3">Government</option>
					<option value="3">Marketing</option>
				</select>
				<button class="button">Buscar</button>
			</div>
		</div>
	</div>
	<!-- end form search -->

	<!-- job category -->
	<div class="job-category app-bg-dark app-section-home">
		<div class="container">
			<div class="app-title">
				<h4>CATEGORIAS</h4>
				<div class="line"></div>
			</div>
			<div class="row">
				<div class="col s6">
					<a href="">
						<div class="entry">
							<i class="fa fa-picture-o"></i>
							<h5>Fotos</h5>
						</div>
					</a>
				</div>
				<div class="col s6">
					<a href="">
						<div class="entry">
							<i class="fa fa-camera"></i>
							<h5>Sesiones</h5>
						</div>
					</a>
				</div>
			</div>
			<div class="row">
				<div class="col s6">
					<a href="">
						<div class="entry">
							<i class="fa fa-music"></i>
							<h5>Baile</h5>
						</div>
					</a>
				</div>
				<div class="col s6">
					<a href="">
						<div class="entry">
							<i class="fa fa-video-camera"></i>
							<h5>Videos</h5>
						</div>
					</a>
				</div>
			</div>
			<div class="row">
				<div class="col s6">
					<a href="">
						<div class="entry">
							<i class="fa fa-film"></i>
							<h5>Peliculas</h5>
						</div>
					</a>
				</div>
				<div class="col s6">
					<a href="">
						<div class="entry">
							<i class="fa fa-bed"></i>
							<h5>Compania</h5>
						</div>
					</a>
				</div>
			</div>
		</div>
	</div>
	<!-- end job category -->

	<!-- fuatured job -->
	<div class="jobs app-section-home">
		<div class="container">
			<div class="app-title">
				<h4>ACTIVIDADES MAS POPULARES</h4>
				<div class="line"></div>
			</div>
			<div class="row">
				<div class="col s6">
					<div class="entry">
						<img src="img/job1.jpg" alt="">
						<div class="content">
							<h5><a href="puesto1.php">Fotos Normales</a></h5>
							<span>2017-06-07</span>
							<i class="fa fa-credit-card"></i> <div class="long">$100</div>
							<div class="location"><i class="fa fa-map-marker"></i>London</div>
						</div>
					</div>
				</div>
				<div class="col s6">
					<div class="entry">
						<img src="img/job2.jpg" alt="">
						<div class="content">
							<h5><a href="puesto2.php">Fotos Sensuales</a></h5>
							<span>2021-04-01</span>
							<i class="fa fa-credit-card"></i> <div class="long">$200</div>
							<div class="location"><i class="fa fa-map-marker"></i>Mexico</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col s6">
					<div class="entry">
						<img src="img/job3.jpg" alt="">
						<div class="content">
							<h5><a href="">Fotos Eroticas</a></h5>
							<span>2017-06-07</span>
							<i class="fa fa-credit-card"></i> <div class="long">$100</div>
							<div class="location"><i class="fa fa-map-marker"></i>London</div>
						</div>
					</div>
				</div>
				<div class="col s6">
					<div class="entry">
						<img src="img/job4.jpg" alt="">
						<div class="content">
							<h5><a href="">Fotos Atrevidas</a></h5>
							<span>2017-06-07</span>
							<i class="fa fa-credit-card"></i> <div class="long">$100</div>
							<div class="location"><i class="fa fa-map-marker"></i>London</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end fuatured job -->

	<!-- jobs promote -->
	<div class="jobs-promote app-section-home">
		<div class="container">
			<h4>Actividades para Ti Cada Dia</h4>
			<p>Encuentra las mejores actividades que puedes realizar en casa</p>
			<a href="faq.php"><button class="button">Leer Mas</button></a>
		</div>
	</div>
	<!-- end jobs promote -->

	<!-- list jobs -->
	<div class="list-jobs app-section-home">
		<div class="container">
			<div class="app-title">
				<h4>NUEVAS ACTIVIDADES</h4>
				<div class="line"></div>
			</div>
			<div class="entry">
				<div class="content">
					<h5><a href="">Fotos Normales</a></h5>
					<span>2017-06-07</span>
					<i class="fa fa-credit-card"></i> <div class="long">$500</div>
					<div class="location"><i class="fa fa-map-marker"></i>London</div>
				</div>
				<div class="content">
					<h5><a href="">Dama de Companya CDMX</a></h5>
					<span>2017-06-07</span>
					<i class="fa fa-credit-card"></i> <div class="long">$1000</div>
					<div class="location"><i class="fa fa-map-marker"></i>CDMX</div>
				</div>
				<div class="content">
					<h5><a href="">Grabacion de Escena</a></h5>
					<span>2017-06-07</span>
					<i class="fa fa-credit-card"></i> <div class="long">$500</div>
					<div class="location"><i class="fa fa-map-marker"></i>CDMX</div>
				</div>
				<div class="content">
					<h5><a href="">Autograbacion de Video</a></h5>
					<span>2017-06-07</span>
					<i class="fa fa-credit-card"></i> <div class="long">$1000</div>
					<div class="location"><i class="fa fa-map-marker"></i>San Francisco</div>
				</div>
			</div>
			<div class="pagination">
				<ul>
					<li><a href="">Primero</a></li>
					<li class="active"><a href="">1</a></li>
					<li><a href="">2</a></li>
					<li><a href="">3</a></li>
					<li><a href="">4</a></li>
					<li><a href="">Ultima</a></li>
				</ul>
			</div>
		</div>
	</div>
	<!-- end list jobs -->
	
	<!-- footer -->
	<?php include 'footer.php'; ?>
	<!-- end footer -->
	
	<!-- script -->
	<script src="js/jquery.min.js"></script>
	<script src="js/materialize.min.js"></script>
	<script src="js/slick.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/custom.js"></script>
	
</body>
</html>