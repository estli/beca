<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Becaria.xyz - Empleos para Chicas</title>
	<meta name="viewport" content="width=device-width, initial-scale=1  maximum-scale=1 user-scalable=no">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="HandheldFriendly" content="True">
	
	<link rel="shortcut icon" href="img/favicon.png">

	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/materialize.min.css">
	<link rel="stylesheet" href="css/slick.css">
	<link rel="stylesheet" href="css/slick-theme.css">
	<link rel="stylesheet" href="css/owl.carousel.css">
	<link rel="stylesheet" href="css/owl.theme.css">
	<link rel="stylesheet" href="css/owl.transitions.css">
	<link rel="stylesheet" href="css/style.css">

</head>
<body>

	<!-- navbar -->
	<?php include 'menu.php'; ?>
	<!-- end panel control left -->

	<!-- list jobs -->
	<div class="list-jobs app-pages app-section">
		<div class="container">
			<div class="entry">
				<div class="content">
					<h5><a href="puesto1.php">Web Developer Senior</a></h5>
					<span>2017-06-07</span>
					<div class="long">FULL TIME</div>
					<div class="location"><i class="fa fa-map-marker"></i>London</div>
				</div>
				<div class="content">
					<h5><a href="">Internet Marketing</a></h5>
					<span>2017-06-07</span>
					<div class="long">PART TIME</div>
					<div class="location"><i class="fa fa-map-marker"></i>Paris</div>
				</div>
				<div class="content">
					<h5><a href="">Website Administrator</a></h5>
					<span>2017-06-07</span>
					<div class="long">FULL TIME</div>
					<div class="location"><i class="fa fa-map-marker"></i>New York</div>
				</div>
				<div class="content">
					<h5><a href="">Web Design and Web Developer</a></h5>
					<span>2017-06-07</span>
					<div class="long">PART TIME</div>
					<div class="location"><i class="fa fa-map-marker"></i>San Francisco</div>
				</div>
			</div>
			<div class="pagination">
				<ul>
					<li><a href="">First</a></li>
					<li class="active"><a href="">1</a></li>
					<li><a href="">2</a></li>
					<li><a href="">3</a></li>
					<li><a href="">4</a></li>
					<li><a href="">Last</a></li>
				</ul>
			</div>
		</div>
	</div>
	<!-- end list jobs -->
	
	<!-- footer -->
	<?php include 'footer.php'; ?>
	<!-- end footer -->
	
	<!-- script -->
	<script src="js/jquery.min.js"></script>
	<script src="js/materialize.min.js"></script>
	<script src="js/slick.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/custom.js"></script>
</body>
</html>