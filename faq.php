<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Becaria.xyz - Empleos para Chicas</title>
	<meta name="viewport" content="width=device-width, initial-scale=1  maximum-scale=1 user-scalable=no">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="HandheldFriendly" content="True">
	
	<link rel="shortcut icon" href="img/favicon.png">

	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/materialize.min.css">
	<link rel="stylesheet" href="css/slick.css">
	<link rel="stylesheet" href="css/slick-theme.css">
	<link rel="stylesheet" href="css/owl.carousel.css">
	<link rel="stylesheet" href="css/owl.theme.css">
	<link rel="stylesheet" href="css/owl.transitions.css">
	<link rel="stylesheet" href="css/style.css">

</head>
<body>

	<!-- navbar -->
	<?php include 'menu.php'; ?>
	<!-- end panel control left -->

	<!-- faq -->
	<div class="faq app-pages app-section">
		<div class="container">
			<div class="pages-title">
				<h3>Preguntas Frecuentes</h3>
			</div>
			<div class="entry">
				<ul class="collapsible" data-collapsible="accordion"> 
					<li>
						<div class="collapsible-header faq-collapsible">
							Que es Becaria.xyz? <i class="fa fa-plus"></i>
						</div>
						<div class="collapsible-body">
							<p>Somos un sitio exclusivo para apoyar a mujeres mexicanas que necesitan ganar dinero!</p>
						</div>
					</li>
					<li>
						<div class="collapsible-header faq-collapsible">
							Como gano dinero? <i class="fa fa-plus"></i>
						</div>
						<div class="collapsible-body">
							<p>Tenemos diferentes actividades que puedes realizar facilmente y al realizarlas obtendras rapidamente el dinero prometido!</p>
						</div>
					</li>
					<li>
						<div class="collapsible-header faq-collapsible">
							Que necesito para ganar dinero? <i class="fa fa-plus"></i>
						</div>
						<div class="collapsible-body">
							<p>Muchas ganas de ganar dinero y una cuenta bancaria a la cual se te realizara el pago!</p>
						</div>
					</li>
					<li>
						<div class="collapsible-header faq-collapsible">
							Como puedo confiar en el sitio? <i class="fa fa-plus"></i>
						</div>
						<div class="collapsible-body">
							<p>El pago se hara una vez que entregues tu actividad!</p>
						</div>
					</li>
					<li>
						<div class="collapsible-header faq-collapsible">
							Que pasara con el material que envie? <i class="fa fa-plus"></i>
						</div>
						<div class="collapsible-body">
							<p>Al realizar el pago, nos cedes los derechos, por lo cual tenemos diferentes actividades donde no es necesario mostrar tu rostro!</p>
						</div>
					</li>
					<li>
						<div class="collapsible-header faq-collapsible">
							Soy hombre? <i class="fa fa-plus"></i>
						</div>
						<div class="collapsible-body">
							<p>Lo sentimos, solo aceptamos mujeres!</p>
						</div>
					</li>
					<li>
						<div class="collapsible-header faq-collapsible">
							Can I buy using paypal? <i class="fa fa-plus"></i>
						</div>
						<div class="collapsible-body">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate illum, accusamus vel dolorem et veritatis. Ab odit quasi libero asperiores at vitae eveniet facere, ea nesciunt, aperiam magnam incidunt delectus!</p>
						</div>
					</li>
					<li>
						<div class="collapsible-header faq-collapsible">
							How to reset password? <i class="fa fa-plus"></i>
						</div>
						<div class="collapsible-body">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate illum, accusamus vel dolorem et veritatis. Ab odit quasi libero asperiores at vitae eveniet facere, ea nesciunt, aperiam magnam incidunt delectus!</p>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>	
	<!-- end faq -->	
	
	<!-- footer -->
	<?php include 'footer.php'; ?>
	<!-- end footer -->
	
	<!-- script -->
	<script src="js/jquery.min.js"></script>
	<script src="js/materialize.min.js"></script>
	<script src="js/slick.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/custom.js"></script>

</body>
</html>