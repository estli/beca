<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Becaria.xyz - Empleos para Chicas</title>
	<meta name="viewport" content="width=device-width, initial-scale=1  maximum-scale=1 user-scalable=no">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="HandheldFriendly" content="True">
	
	<link rel="shortcut icon" href="img/favicon.png">

	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/materialize.min.css">
	<link rel="stylesheet" href="css/slick.css">
	<link rel="stylesheet" href="css/slick-theme.css">
	<link rel="stylesheet" href="css/owl.carousel.css">
	<link rel="stylesheet" href="css/owl.theme.css">
	<link rel="stylesheet" href="css/owl.transitions.css">
	<link rel="stylesheet" href="css/style.css">

</head>
<body>

	<!-- navbar -->
	<?php include 'menu.php'; ?>
	<!-- end panel control left -->

	<!-- details -->
	<div class="details app-pages app-section">
		<div class="container">
			<img src="img/details.jpg" alt="">
			<h3>Junior Web Developer</h3>
			<span>2017-06-07</span>
			<i class="fa fa-credit-card"></i> <div class="long">$100</div>
			<div class="location"><i class="fa fa-map-marker"></i>London</div>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium ducimus ipsam, suscipit, nobis necessitatibus eum odio est laborum fugiat veniam explicabo qui laboriosam! Repellat aspernatur doloribus, nulla, hic odio natus!</p>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Impedit ex, saepe explicabo similique eaque atque voluptatum earum quo minus, aliquam repellat excepturi tenetur reprehenderit id sit, in nam temporibus ipsa.</p>
			<?php include 'postula.php'; ?>
		</div>
	</div>	
	<!-- end details -->	
	
	<!-- footer -->
	<?php include 'footer.php'; ?>
	<!-- end footer -->
	
	<!-- script -->
	<script src="js/jquery.min.js"></script>
	<script src="js/materialize.min.js"></script>
	<script src="js/slick.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/custom.js"></script>

</body>
</html>