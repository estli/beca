<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Becaria.xyz - Empleos para Chicas</title>
	<meta name="viewport" content="width=device-width, initial-scale=1  maximum-scale=1 user-scalable=no">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="HandheldFriendly" content="True">
	
	<link rel="shortcut icon" href="img/favicon.png">

	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/materialize.min.css">
	<link rel="stylesheet" href="css/slick.css">
	<link rel="stylesheet" href="css/slick-theme.css">
	<link rel="stylesheet" href="css/owl.carousel.css">
	<link rel="stylesheet" href="css/owl.theme.css">
	<link rel="stylesheet" href="css/owl.transitions.css">
	<link rel="stylesheet" href="css/style.css">

</head>
<body>

	<!-- navbar -->
	<?php include 'menu.php'; ?>
	<!-- end panel control left -->

	<!-- details -->
	<div class="details app-pages app-section">
		<div class="container">
			<img src="img/details.jpg" alt="">
			<h3>Asistente General</h3>
			<span>2021-04-01</span>
			<i class="fa fa-credit-card"></i> <div class="long">$100</div>
			<div class="location"><i class="fa fa-map-marker"></i>Mexico</div>
			<p>Edad: Mayor de 18 anos</p>
			
			<p>Solicitamos chica que este dispuesta a mostrar su cuerpo</p>
			
			<p>Pago por sesion de fotos y videos</p>

			<p></p>
			<?php include 'postula.php'; ?>
			
		</div>
	</div>	
	<!-- end details -->	
	
	<!-- footer -->
	<?php include 'footer.php'; ?>
	<!-- end footer -->
	
	<!-- script -->
	<script src="js/jquery.min.js"></script>
	<script src="js/materialize.min.js"></script>
	<script src="js/slick.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/custom.js"></script>

</body>
</html>